package ar.uba.fi.tdd.exercise;

class ShopItemFactory {

	public static ShopItem create(Item item) {
		switch(item.Name) {
			case "Aged Brie":
				return new AgingItem(item);
			case "Backstage passes to a TAFKAL80ETC concert":
				return new EventItem(item);
			case "Sulfuras, Hand of Ragnaros":
				return new LegendaryItem(item);
			case "The Conjured Item":
				return new ConjuredItem(item);
			default:
				return new CommonItem(item);
		}
	}
}