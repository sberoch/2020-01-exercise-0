package ar.uba.fi.tdd.exercise;

class CommonItem extends ShopItem {

	public CommonItem(Item item) {
		this.item = item;
		if (item.quality > 50) {
			throw new IllegalStateException("Items cannot have over 50 quality.");
		}
	}

	void updateQuality() {
		if (this.item.sellIn > 0 && this.item.quality > 0) {
			this.item.sellIn--;
			this.item.quality--;

		} else if (this.item.sellIn == 0 && this.item.quality > 1) {
			this.item.quality -= 2;

		} else if (this.item.sellIn == 0 && this.item.quality > 0) {
			this.item.quality--;
			
		} else {
			return;
		}
	}
}