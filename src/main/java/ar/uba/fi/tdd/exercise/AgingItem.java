package ar.uba.fi.tdd.exercise;

class AgingItem extends ShopItem {

	public AgingItem(Item item) {
		this.item = item;
		if (item.quality > 50) {
			throw new IllegalStateException("Items cannot have over 50 quality.");
		}
	}

	void updateQuality() {
		if (this.item.quality < 50) {
			this.item.quality++;
		}
		
		if (this.item.sellIn > 0) {
			this.item.sellIn--;
		}
	}
}