package ar.uba.fi.tdd.exercise;

class ConjuredItem extends ShopItem {

	public ConjuredItem(Item item) {
		this.item = item;
		if (item.quality > 50) {
			throw new IllegalStateException("Items cannot have over 50 quality.");
		}
	}

	void updateQuality() {
		if (this.item.sellIn > 0) {
			this.item.sellIn--;
			this.item.quality -= 2;

		} else if (this.item.sellIn == 0) {
			this.item.quality -= 4;
		} 

		if (this.item.quality < 0) {
			this.item.quality = 0;
		}
	}
}