
package ar.uba.fi.tdd.exercise;
import java.util.List;
import java.util.ArrayList;

class GildedRose {
    Item[] items;
    List<ShopItem> shopItems;

    public GildedRose(Item[] _items) {
        items = _items;
        shopItems = new ArrayList();
        for (Item item : items) {
            shopItems.add(ShopItemFactory.create(item));
        }
    }

    public void updateQuality() {
        for (ShopItem item : shopItems) {
            item.updateQuality();
        }
    }
}
