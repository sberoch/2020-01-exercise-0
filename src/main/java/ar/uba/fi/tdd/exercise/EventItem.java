package ar.uba.fi.tdd.exercise;

class EventItem extends ShopItem {

	public EventItem(Item item) {
		this.item = item;
		if (item.quality > 50) {
			throw new IllegalStateException("Items cannot have over 50 quality.");
		}
	}

	void updateQuality() {
		int incAmount = 1;
		if (this.item.sellIn <= 10 && this.item.sellIn > 5) {
			incAmount = 2;
		} else if (this.item.sellIn <= 5) {
			incAmount = 3;
		}

		this.item.quality += incAmount;
		if (this.item.quality > 50) {
			this.item.quality = 50;
		}

		if (this.item.sellIn > 0) {
			this.item.sellIn--;
		} else {
			this.item.quality = 0;
		}
	}
}