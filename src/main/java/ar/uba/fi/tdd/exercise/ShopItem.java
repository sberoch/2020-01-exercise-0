package ar.uba.fi.tdd.exercise;

abstract class ShopItem {
	protected Item item;
	abstract void updateQuality();
}