package ar.uba.fi.tdd.exercise;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GildedRoseTest {

    @Test
    public void agedBrieShouldNotHaveQualityOverFiftyInitially() {
        Item[] items = new Item[] { new Item("Aged Brie", 10, 60) };
        boolean thrown = false;
        try {
            GildedRose app = new GildedRose(items);
        } catch (IllegalStateException e) {
            thrown = true;
        }
        assertThat(thrown).isEqualTo(true);
    }

    @Test
    public void updateAgedBrieQualityShouldIncreaseIt() {
        Item[] items = new Item[] { new Item("Aged Brie", 10, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(11);
    }

    @Test
    public void updateAgedBrieQualityShouldDecreaseSellIn() {
        Item[] items = new Item[] { new Item("Aged Brie", 10, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].sellIn).isEqualTo(9);
    }

    @Test
    public void agedBrieSellInShouldNotBeNegative() {
        Item[] items = new Item[] { new Item("Aged Brie", 0, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].sellIn).isEqualTo(0);
        assertThat(app.items[0].quality).isEqualTo(11);
    }

    @Test
    public void agedBrieQualityShouldNotGoOverFifty() {
        Item[] items = new Item[] { new Item("Aged Brie", 10, 50) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(50);
    }

    @Test
    public void updateSulfurasShouldRemainTheSame() {
        Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 80) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(80);
        assertThat(app.items[0].sellIn).isEqualTo(10);
    }

    @Test
    public void backstagePassesShouldNotHaveQualityOverFiftyInitially() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 10, 60) };
        boolean thrown = false;
        try {
            GildedRose app = new GildedRose(items);
        } catch (IllegalStateException e) {
            thrown = true;
        }
        assertThat(thrown).isEqualTo(true);
    }

    @Test
    public void backstagePassesQualityShouldIncreaseByOneWhenMoreThanTenDays() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 11, 20) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(21);
        assertThat(app.items[0].sellIn).isEqualTo(10);
    }

    @Test
    public void backstagePassesQualityShouldIncreaseByTwoWhenTenDaysOrLess() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 10, 20) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(22);
        assertThat(app.items[0].sellIn).isEqualTo(9);
    }

    @Test
    public void backstagePassesQualityShouldIncreaseByThreeWhenFiveDaysOrLess() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 20) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(23);
        assertThat(app.items[0].sellIn).isEqualTo(4);
    }

    @Test
    public void backstagePassesQualityShouldBeZeroWhenConcertHappens() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 20) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(0);
    }

    @Test
    public void backstagePassesSellInShouldNotBeNegative() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 20) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].sellIn).isEqualTo(0);
    }

    @Test
    public void backstagePassesQualityShouldNotGoOverFifty() {
        Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 2, 49) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(50);
    }

    @Test
    public void commonItemShouldNotHaveQualityOverFiftyInitially() {
        Item[] items = new Item[] { new Item("Common item", 10, 60) };
        boolean thrown = false;
        try {
            GildedRose app = new GildedRose(items);
        } catch (IllegalStateException e) {
            thrown = true;
        }
        assertThat(thrown).isEqualTo(true);
    }

    @Test
    public void updateCommonItemQualityShouldDecrease() {
        Item[] items = new Item[] { new Item("Common item", 10, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(9);
    }

    @Test
    public void commonItemSellInShouldNotBeNegative() {
        Item[] items = new Item[] { new Item("Common item", 0, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].sellIn).isEqualTo(0);
    }

    @Test
    public void commonItemQualityShouldNotBeNegative() {
        Item[] items = new Item[] { new Item("Common item", 1, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(0);
    }

    @Test
    public void commonItemQualityShouldDecreaseFasterWhenSellInIsZero() {
        Item[] items = new Item[] { new Item("Common item", 0, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(8);
    }

    @Test
    public void conjuredItemShouldNotHaveQualityOverFiftyInitially() {
        Item[] items = new Item[] { new Item("The Conjured Item", 10, 60) };
        boolean thrown = false;
        try {
            GildedRose app = new GildedRose(items);
        } catch (IllegalStateException e) {
            thrown = true;
        }
        assertThat(thrown).isEqualTo(true);
    }

    @Test
    public void conjuredItemQualityShouldDecreaseTwiceAsFastAsCommonItems() {
        Item[] items = new Item[] { new Item("The Conjured Item", 10, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(8);
    }

    @Test
    public void conjuredItemQualityShouldNotBeNegative() {
        Item[] items = new Item[] { new Item("The Conjured Item", 10, 0) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(0);
    }

    @Test
    public void conjuredItemSellInShouldNotBeNegative() {
        Item[] items = new Item[] { new Item("The Conjured Item", 0, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].sellIn).isEqualTo(0);
    }

    @Test
    public void conjuredItemQualityShouldDecreaseByFourWhenSellInIsZero() {
        Item[] items = new Item[] { new Item("The Conjured Item", 0, 10) };
        GildedRose app = new GildedRose(items);
        app.updateQuality();
        assertThat(app.items[0].quality).isEqualTo(6);
    }
}
